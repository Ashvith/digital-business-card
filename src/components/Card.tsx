import Portrait from '../images/Photo.png'
import CardBody from './CardBody'
import CardFooter from './CardFooter'

function Card() {
  return (
    <div className='card'>
      <img src={Portrait} alt='' className='card--image' />
      <CardBody />
      <CardFooter />
    </div>
  )
}

export default Card
