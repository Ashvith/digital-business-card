import CardBodyHeader from './CardBodyHeader'
import CardBodyDescription from './CardBodyDescription'

function CardBody() {
  return (
    <div className='card--body'>
      <CardBodyHeader />
      <CardBodyDescription />
    </div>
  )
}

export default CardBody
