import MailIcon from '../images/Mail.png'
import LinkedInIcon from '../images/Linkedin.png'
function CardHeader() {
  return (
    <div className='card--header'>
      <h1 className='card--header--title'>Ashvith Shetty</h1>
      <h3 className='card--header--sub-title'>Fullstack Developer</h3>
      <div className='card--header--website'>ashvith.codes</div>
      <div className='card--header--button-group'>
        <button className='card--header--button-group--email'>
          <img src={MailIcon} />
          Email
        </button>
        <button className='card--header--button-group--linkedin'>
          <img src={LinkedInIcon} />
          LinkedIn
        </button>
      </div>
    </div>
  )
}

export default CardHeader
