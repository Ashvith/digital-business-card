function CardDescription() {
  return (
    <div className='card--description'>
      <section className='card--description--about'>
        <h3>About</h3>
        <p>
          I am a full-stack developer with a particular interest in making things simple and
          automating daily tasks. I try to keep up with security and best practices, and am always
          looking for new things to learn.
        </p>
      </section>
      <section className='card--description--interests'>
        <h3>Interests</h3>
        <p>Foodie. Reader. Entrepreneur. Tech geek. Coffee fanatic.</p>
      </section>
    </div>
  )
}

export default CardDescription
