import GitHubIcon from '../images/GitHub Icon.png'
import FacebookIcon from '../images/Facebook Icon.png'
import TwitterIcon from '../images/Twitter Icon.png'
import InstagramIcon from '../images/Instagram Icon.png'

function CardFooter() {
  return (
    <div className='card--footer'>
      <img src={TwitterIcon} />
      <img src={FacebookIcon} />
      <img src={InstagramIcon} />
      <img src={GitHubIcon} />
    </div>
  )
}

export default CardFooter
